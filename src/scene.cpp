
void parse_scenes(std::unordered_map<std::string, Scene> &scenes, std::string scenes_path, Asset_Storage *assets) {
    scenes.clear();

    QFile scene_file(QString::fromStdString(scenes_path));
    if (scene_file.open(QFile::ReadOnly)) {
        while (!scene_file.atEnd()) {
            Scene scene;
            auto line = scene_file.readLine().toStdString();
            std::stringstream ss(line);

            std::string background_name;
            ss >> scene.name >> background_name;
            trim(background_name);

            if (assets->name_to_id.find(background_name) != assets->name_to_id.end()) {
                scene.background.asset_id = assets->name_to_id[background_name];
            } else {
                scene.background.asset_id = 0;
            }
            scenes.insert(std::make_pair(scene.name, scene));
        }
    }
}

Asset_Storage *load_assets(const std::string path) {
    auto storage = new Asset_Storage;

    // Load images
    QDir images_dir(QString::fromStdString(path) + "/images/");
    images_dir.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );
    auto images_names = images_dir.entryList();
    auto images_count = images_names.size();

    if (!images_count)  return nullptr;

    storage->images.reserve(images_count);

    // Zero index is not used
    storage->images.push_back(QPixmap());

    for (auto &image_name: images_names) {
        auto image_path = images_dir.filePath(image_name);
        storage->images.push_back(QPixmap(image_path));
        storage->name_to_id.insert(std::make_pair<std::string, int>(image_name.toStdString(), storage->images.size() - 1));
    }

    // Load characters
    QFile characters_file(QString::fromStdString(path) + "/characters.txt");
    if (characters_file.open(QFile::ReadOnly)) {
        while (!characters_file.atEnd()) {
            auto line = characters_file.readLine().toStdString();
            std::stringstream ss(line);

            int r = 255, g = 255, b = 255;
            ss >> r >> g >> b;

            std::string character_name;
            std::getline(ss, character_name);
            trim(character_name);

            if (character_name.length() > 0) {
                storage->character_colors.insert(std::make_pair(character_name, QColor(r, g, b)));
            }
        }
    }

    return storage;
}

inline QPixmap get_image_asset(Asset_Storage *storage, int asset_id) {
    if (asset_id < storage->images.size()) {
        return storage->images[asset_id];
    } else {
        return storage->images[0];
    }
}

void Scene::draw(QPainter &painter, QRect rect, Asset_Storage *storage) {
    painter.drawPixmap(rect, get_image_asset(storage, background.asset_id));
}
