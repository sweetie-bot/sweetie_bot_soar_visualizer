#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QPainter>
#include <QTimer>
#include <QPainter>
#include <QTimer>
#include <QFontDatabase>
#include <QApplication>

#include <ros/ros.h>
#include <ros/package.h>

#include "sweetie_bot_text_msgs/SpeechVisualization.h"

#include "visual_novel.h"

class MainWindow : public QWidget {
    Q_OBJECT

private:
    QTimer m_ros_timer;

    ros::NodeHandle nh;
    ros::Subscriber sub_control;

    VN_State game;

public:
    MainWindow(QWidget *parent = 0);
    
    void paintEvent(QPaintEvent *e);

private slots:
    void updateSceneCallback(const sweetie_bot_text_msgs::SpeechVisualization::ConstPtr &msg);
    void rosSpin();

};


#endif // MAINWINDOW_H
