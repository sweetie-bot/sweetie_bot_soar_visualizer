#ifndef VISUAL_NOVEL_H
#define VISUAL_NOVEL_H

// Std
#include <string>
#include <sstream>
#include <unordered_map>

// Qt
#include <QApplication>
#include <QRect>
#include <QPainter>
#include <QFontDatabase>
#include <QFontMetrics>
#include <QDir>

// Ros
#include <ros/ros.h>
#include <ros/package.h>

// SB
#include "sweetie_bot_text_msgs/SpeechVisualization.h"

#include "text_rendering.h"
#include "scene.h"
#include "utils.h"

struct VN_State {
    std::string m_package_path;

    QFont m_text_font;

    enum Game_State {
      NOT_STARTED,
      RUNNING,
      FINISHED,
    };
    Game_State m_game_state = NOT_STARTED;

    std::string m_default_background;
    Text_Buffer m_text_buffer;

    Asset_Storage *m_assets;
    std::unordered_map<std::string, Scene> m_scenes;

    std::string m_current_scene;

    void init(QRect window_rect);

    void draw(QPainter &painter, QRect rect);
    void update(const sweetie_bot_text_msgs::SpeechVisualization::ConstPtr &msg);

    inline QString getFontAssetPath(const std::string &asset_name) {
        return QString::fromStdString(m_package_path) + QString("/assets/fonts/") + asset_name.c_str();
    }

    inline QString getImageAssetPath(const std::string &asset_name) {
        return QString::fromStdString(m_package_path) + QString("/assets/images/") + asset_name.c_str();
    }
};

#endif // VISUAL_NOVEL_H
