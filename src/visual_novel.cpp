#include "visual_novel.h"

#include "text_rendering.cpp"
#include "scene.cpp"

void VN_State::init(QRect window_rect) {
    m_package_path = ros::package::getPath("sweetie_bot_soar_visualizer");
    m_default_background = "default.jpeg";

    // Load font
    int font_id = QFontDatabase::addApplicationFont(getFontAssetPath("comic-sans-ms.ttf"));
    QString font_family = QFontDatabase::applicationFontFamilies(font_id).at(0);


    int font_size = 40;
    if (ros::param::has("~font_size")) {
        ros::param::get("~font_size", font_size);
    }

    if (ros::param::has("~default_background")) {
        ros::param::get("~default_background", m_default_background);
    }

    m_text_font = QFont(font_family, font_size);
    // m_text_font.setStyleStrategy(QFont::ForceOutline);

    m_assets = load_assets(m_package_path + "/assets/");
    parse_scenes(m_scenes, m_package_path + "/assets/scenes.txt", m_assets);

    std::cout << "Loaded assets: " << std::endl;
    for (auto &pair: m_assets->name_to_id) {
        std::cout << pair.second << " " << pair.first << std::endl;
    }
    std::cout << std::endl;

    std::cout << "Loaded colors: " << std::endl;
    for (auto &pair: m_assets->character_colors) {
        std::cout << pair.first << " " << pair.second.red() <<
                                   " " << pair.second.green() << 
                                   " " << pair.second.blue() << std::endl;
    }
    std::cout << std::endl;

    std::cout << "Loaded scenes: " << std::endl;
    for (auto &pair: m_scenes) {
        auto &scene = pair.second;
        std::cout << "Scene name: " << scene.name
                  << " Background id: " << scene.background.asset_id
                  << std::endl;
    }
}

void VN_State::draw(QPainter &painter, QRect rect) {
    if (m_game_state == NOT_STARTED) {
        painter.drawPixmap(rect, getImageAssetPath(m_default_background));
        return;
    }

    if (m_current_scene.length() > 0) {
        m_scenes[m_current_scene].draw(painter, rect, m_assets);
    }

    if (m_game_state == RUNNING) {
        const int x = 20;
        const int y = rect.height() - 450;
        const int rwidth = rect.width() - x - 20;
        const int rheight = rect.height() - y - 20;
        QRect text_rect(x, y, rwidth, rheight);

        graphical_text_overflow_check(&m_text_buffer, text_rect, m_text_font);

        draw_text_buffer(painter, &m_text_buffer, text_rect, m_text_font);
    }
}

void VN_State::update(const sweetie_bot_text_msgs::SpeechVisualization::ConstPtr &msg) {
    m_game_state = RUNNING;

    if (msg->scene.length() > 0) {
        m_current_scene = msg->scene;
    }

    std::string character_name("default");
    if (msg->character.length() > 0) {
        character_name = msg->character;
    }

    if (m_current_scene.find("end") != std::string::npos) {
        m_game_state = FINISHED;
        return;
    }

    // Reset visualizer
    if (m_current_scene == "default") {
        clear_text_buffer(&m_text_buffer);
        m_game_state = NOT_STARTED;
        return;
    }

    auto character_color = m_assets->character_colors[character_name];
    push_colored_text(&m_text_buffer, msg->text, character_color); 
}

