#ifndef SCENE_H
#define SCENE_H

struct Asset_Storage {
    std::unordered_map<std::string, int> name_to_id;
    std::vector<QPixmap> images;

    std::unordered_map<std::string, QColor> character_colors;
};

Asset_Storage *load_assets(const std::string path);

struct Background {
    int asset_id;
};

struct Sprite {
    int asset_id;
    float x_relative;
    float y_relative;
    float scale;
};

struct Scene {
    std::string name;
    Background background;

    void draw(QPainter &painter, QRect rect, Asset_Storage *storage);
};

void parse_scenes(std::unordered_map<std::string, Scene> &scenes, std::string scenes_path, Asset_Storage *assets);

#endif // SCENE_H
