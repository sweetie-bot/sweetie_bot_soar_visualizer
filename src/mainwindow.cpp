#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    resize(1280, 720);

    game.init(rect());

    sub_control = nh.subscribe<sweetie_bot_text_msgs::SpeechVisualization>("speech_visualization", 1, &MainWindow::updateSceneCallback, this);

    connect(&m_ros_timer, SIGNAL(timeout()), this, SLOT(rosSpin()));
    m_ros_timer.start(10);
}

void MainWindow::rosSpin()
{
    if(!ros::ok()) QApplication::quit();
    ros::spinOnce();
}


void MainWindow::paintEvent(QPaintEvent *e) {
    Q_UNUSED(e);

    QPainter painter(this);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    game.draw(painter, rect());

    painter.end();
}

void MainWindow::updateSceneCallback(const sweetie_bot_text_msgs::SpeechVisualization::ConstPtr &msg) {
    game.update(msg);

    update();
}
