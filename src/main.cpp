#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    ros::init(argc, argv, "soar_visualizer");

    MainWindow w;
    w.show();

    return a.exec();
}
