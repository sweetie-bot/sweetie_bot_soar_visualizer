
void push_colored_text(Text_Buffer *buffer, const std::string &text, QColor color) {
    buffer->phrases.push_back(text);
    buffer->colors.push_back(color);
}

void graphical_text_overflow_check(Text_Buffer *buffer, QRect bounding_rect, QFont &font) {
    // Get total hight of buffer
    int buffer_total_height = 0;
    QFontMetrics metrics(font);
    auto current_boundary = bounding_rect;
    for (int i = 0; i < buffer->phrases.size(); i++) {
        QRect rect = metrics.boundingRect(current_boundary, Qt::TextWordWrap, buffer->phrases[i].c_str());
        buffer_total_height += rect.height();
        current_boundary.setY(current_boundary.y() + rect.height());
    }

    auto last_phrase = buffer->phrases.back();
    auto last_color  = buffer->colors.back();

    // If overflow detected reset the buffer except the last phrase
    const int pad = 20;
    if (buffer_total_height >= bounding_rect.height() - pad) {
        clear_text_buffer(buffer);
        push_colored_text(buffer, last_phrase, last_color);
    }
}

void draw_text_buffer(QPainter &painter, Text_Buffer *buffer, QRect boundary, QFont &font) {
    const int pad = 20;
    QRect boundary_pad(boundary.x() + pad,
                       boundary.y() + pad,
                       boundary.width() - 2*pad,
                       boundary.height() - 2*pad);

    painter.setBrush(QColor(71, 45, 37, 200));
    painter.drawRoundedRect(boundary, 10, 10);

    painter.setFont(font);

    QFontMetrics metrics(font);
    auto current_boundary = boundary_pad;
    for (int i = 0; i < buffer->phrases.size(); i++) {
        painter.setPen(QPen(buffer->colors[i]));
        painter.drawText(current_boundary, Qt::TextWordWrap, buffer->phrases[i].c_str());

        QRect rect = metrics.boundingRect(current_boundary, Qt::TextWordWrap, buffer->phrases[i].c_str());
        current_boundary.setY(current_boundary.y() + rect.height());
    }

    // Restore default painter pen
    painter.setPen(QPen(QColor(Qt::white)));
}

void clear_text_buffer(Text_Buffer *buffer) {
    buffer->phrases.clear();
    buffer->colors.clear();
}

int total_buffer_length_with_spaces(Text_Buffer *buffer) {
    int total_length = 0;
    for (auto &phrase: buffer->phrases) {
        total_length += phrase.length();
    }
    return total_length + buffer->phrases.size();
}
