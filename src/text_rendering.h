#ifndef TEXT_RENDERING_H
#define TEXT_RENDERING_H


static const int max_buffer_length = 1024;

struct Text_Buffer {
    std::vector<std::string> phrases;
    std::vector<QColor> colors;
};

void draw_text_buffer(QPainter &painter, Text_Buffer *buffer, QRect boundary, QFont &font);
void push_text(Text_Buffer *buffer, const std::string &text);
void clear_text_buffer(Text_Buffer *buffer);
int total_buffer_length_with_spaces(Text_Buffer *buffer);

#endif
